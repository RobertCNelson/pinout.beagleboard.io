<!--
---
name: socpin
class: interface
type: pinout
page_url: socpin
url: https://github.com/socpin/
github: https://github.com/socpin/socpin-Python
pin:
  '3':
    name: socpin 8
  '5':
    name: socpin 9
  '7':
    name: socpin 7
  '8':
    name: socpin 15
  '10':
    name: socpin 16
  '11':
    name: socpin 0
  '12':
    name: socpin 1
  '13':
    name: socpin 2
  '15':
    name: socpin 3
  '16':
    name: socpin 4
  '18':
    name: socpin 5
  '19':
    name: socpin 12
  '21':
    name: socpin 13
  '22':
    name: socpin 6
  '23':
    name: socpin 14
  '24':
    name: socpin 10
  '26':
    name: socpin 11
  '27':
    name: socpin 30
  '28':
    name: socpin 31
  '29':
    name: socpin 21
  '31':
    name: socpin 22
  '32':
    name: socpin 26
  '33':
    name: socpin 23
  '35':
    name: socpin 24
  '36':
    name: socpin 27
  '37':
    name: socpin 25
  '38':
    name: socpin 28
  '40':
    name: socpin 29
-->
# socpin

socpin is an attempt to bring Arduino-wiring-like simplicity to the Raspberry Pi.

The goal is to have a single common platform and set of functions for accessing the Raspberry Pi GPIO across multiple languages. socpin is a C library at heart, but it's available to both Ruby and Python users who can "gem install socpin" or "pip install socpin" respectively.

socpin uses its own pin numbering scheme, this page illustrates how socpin numbers your GPIO pins.

socpin has been deprecated by its original author. For more information about, and support with the ongoing community maintained version and ports, see the socpin GitHub org: https://github.com/socpin/